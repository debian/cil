cil (0.07.00-12) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:49:51 +0200

cil (0.07.00-11) unstable; urgency=medium

  * New debian/patches/0700-getopt_mixed.patch to remove the deprecated
    dependency libgetopt-mixed-perl (Closes: #829577).
  * debian/copyright:
    - Add tag for debian/patches/0700-getopt_mixed.patch.
    - Add year 2016 to myself at debian/*
  * debian/control:
    - Bump debhelper Build-Depends minimum version to 10.
    - Change Vcs-Browser to secure URI.
    - Bump Standards-Version to 3.9.8 (no changes required).
    - Move libmodule-build-perl from Build-Depends-Indep to Build-Depends
      to prevent lintian warning.
  * debian/compat:
    - Bump compat level to 10.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 20 Nov 2016 09:29:04 +0100

cil (0.07.00-10) unstable; urgency=medium

  * Move bash_completion from etc/bash_completion.d to
    usr/share/bash-completion/completions
  * New debian/cil.maintscript:
    - Remove old etc/bash_completion.d/cil.
  * debian/control:
    - Add Build-Depends-Indep libmodule-build-perl to prevent
      FTBFS with perl >= 5.22 (Closes: #788890).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 16 Jun 2015 12:07:22 +0200

cil (0.07.00-9) unstable; urgency=low

  * debian/control:
    - Update deprecated Vcs-* links.
    - Bump Standards-Version to 3.9.6 (no changes required).
  * Update debian/watch.
  * Rewrite debian/copyright to DEP-5 format.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Mon, 27 Apr 2015 09:20:11 +0200

cil (0.07.00-8) unstable; urgency=low

  * remove redundant manpages (closes: #746022)

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 29 Apr 2014 18:10:46 +0200

cil (0.07.00-7) unstable; urgency=low

  * add override_dh_autoclean to debian/rules
  * add upstream-changelog
  * Add missing build-dependencies (libdatetime-perl)
  * Bump Standards-Version to 3.9.5
  * New maintainer (closes: #744362)

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 13 Apr 2014 20:09:19 +0200

cil (0.07.00-6) unstable; urgency=medium

  [ Michael Gilbert ]
  * Add missing build-dependencies (closes: #702646).

  [ Gianluca Ciccarelli ]
  * Bump Standards-Version to 3.9.4
  * Fix Lintian warning about Vcs-Git being not canonical
  * Change Homepage: (closes: #702650)

 -- Gianluca Ciccarelli <galiziacentrale@gmail.com>  Fri, 15 Mar 2013 21:03:50 +0100

cil (0.07.00-5) unstable; urgency=low

  [ Francois Marier ]
  * Fix watch file (project moved to github)

  [ Gianluca Ciccarelli ]
  * New maintainer (closes: #674829)

 -- Gianluca Ciccarelli <galiziacentrale@gmail.com>  Sun, 17 Feb 2013 16:26:58 +0100

cil (0.07.00-4) unstable; urgency=low

  * Switch to minimal debian/rules file
  * Remove co-maintainer

  * Bump debhelper version to 9
  * Bump Standards-Version up to 3.9.3

 -- Francois Marier <francois@debian.org>  Mon, 28 May 2012 15:28:06 +1200

cil (0.07.00-3) unstable; urgency=medium

  * Fix FTBFS due to weird upstream Makefile (closes: #601935)
    Thanks Fabrice for the patch!
  * Bump debhelper version to 8

 -- Francois Marier <francois@debian.org>  Sun, 31 Oct 2010 21:43:32 +1300

cil (0.07.00-2) unstable; urgency=medium

  * Add missing dependency on libfile-homedir-perl (closes: #593520)

 -- Francois Marier <francois@debian.org>  Wed, 18 Aug 2010 20:21:05 -0400

cil (0.07.00-1) unstable; urgency=low

  * New upstream release
  * Swap maintainer and uploaders
  * Bump Standards-Version up to 3.9.1

 -- Francois Marier <francois@debian.org>  Thu, 05 Aug 2010 20:58:12 -0400

cil (0.5.1-3) unstable; urgency=low

  * Bump Standards-Version up to 3.8.4
  * Update homepage URL (including the watch file) to point to chilts.org
  * Switch to 3.0 (quilt) source format
  * Refer to the right version of the GPL (v3) in debian/copyright

 -- Francois Marier <francois@debian.org>  Thu, 11 Feb 2010 14:51:10 +1300

cil (0.5.1-2) unstable; urgency=low

  * Updated watch file (upstream filename changed)
  * Bump Standards-Version up to 3.8.1 (no changes)
  * Add a dependency on misc:Depends (lintian warning)
  * Bump debhelper dependency to 7 and add call to dh_prep

 -- Francois Marier <francois@debian.org>  Fri, 08 May 2009 09:20:06 +1200

cil (0.5.1-1) unstable; urgency=low

  [ Andrew Chilton ]
  * New Upstream Version

  [ Francois Marier ]
  * Change priority to extra
  * Install bash completion script
  * Install upstream changelog

 -- Francois Marier <francois@debian.org>  Sat, 05 Jul 2008 21:13:00 +1200

cil (0.5.0) unstable; urgency=low

  [ Andrew Chilton ]
  * New Upstream Version

  [ Francois Marier ]
  * Add dependency on libemail-{simple,date,find}-perl
  * Update watch file (renamed upstream tarballs)

 -- Francois Marier <francois@debian.org>  Sat, 05 Jul 2008 18:01:00 +1200

cil (0.2.1-2) unstable; urgency=low

  * Remove libterm-calleditor-perl dependency
  * Cherry-pick upstream bugfix (6424e2cf7ddbd34f6bbf6b94c3489d6909c183c3)

 -- Francois Marier <francois@debian.org>  Tue, 24 Jun 2008 16:50:02 +1200

cil (0.2.1-1) unstable; urgency=low

  * Initial release (closes: #487593)

 -- Francois Marier <francois@debian.org>  Mon, 23 Jun 2008 23:43:50 +1200
